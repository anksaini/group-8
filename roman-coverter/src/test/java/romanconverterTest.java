import org.junit.Test;
import roman.RomanConverter;
import static org.junit.Assert.assertEquals;


public class romanconverterTest {
    @Test
    public void test1(){
        String a = "X";
        RomanConverter rc1 = new RomanConverter();
        assertEquals(rc1.fromRoman(a), 10);
    }
    @Test
    public void test2(){
        int b = 40;
        RomanConverter rc2 = new RomanConverter();
        assertEquals(rc2.toRoman(b),"XL");
    }
    @Test
    public void test3(){
        int c = 48;
        RomanConverter rc3 = new RomanConverter();
        assertEquals(rc3.toRoman(c),"XLVIII");
    }
    @Test(expected = IllegalArgumentException.class)
    public void test4() throws Exception{
        RomanConverter rc4 = new RomanConverter();
        rc4.toRoman(4001);
    }
    @Test(expected = IllegalArgumentException.class)
    public void test5() throws Exception{
        RomanConverter rc5 = new RomanConverter();
        rc5.fromRoman("AAAA");
    }
    @Test(expected = IllegalArgumentException.class)
    public void test6() throws Exception{
        RomanConverter rc6 = new RomanConverter();
        rc6.toRoman(-1);
    }
}
